package com.lyson.steam.price.tracker.service;

import com.lyson.steam.price.tracker.domain.PriceEntry;
import com.lyson.steam.price.tracker.dao.PriceEntryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lyson on 01.06.2016.
 */
@Service
public class SteamPriceEntryService implements PriceEntryService {

    @Autowired
    private PriceEntryDao priceEntryDao;

    @Override
    public void addPriceEntry(PriceEntry priceEntry) {
        priceEntryDao.add(priceEntry);
    }

    @Override
    public List<PriceEntry> findPricesForGame(String gameSteamId) {
        return priceEntryDao.findPricesForGame(Long.valueOf(gameSteamId));
    }
}
