package com.lyson.steam.price.tracker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lyson on 31.05.2016.
 */
@Controller
@RequestMapping("/games")
public class SteamGamesViewController {

    @RequestMapping(method = RequestMethod.GET)
    public String getGames(Model model) {
        return "games";
    }

}