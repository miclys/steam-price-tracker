package com.lyson.steam.price.tracker.service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lyson.steam.price.tracker.domain.Game;
import com.lyson.steam.price.tracker.dao.GameDao;
import com.lyson.steam.price.tracker.domain.PriceEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by lyson on 01.06.2016.
 */
@Service
public class SteamGameService implements GameService {

    private final static String gameDetailsUrl = "http://store.steampowered.com/api/appdetails?appids=";


    @Autowired
    private GameDao gameDao;
    @Autowired
    private SteamPriceEntryService priceEntryService;


    @Override
    public void addGame(Game game) {
        final Optional<JsonObject> gameJson = requestSteamForGameDetails(game.getSteamId());
        if (gameJson.isPresent() && gameJson.get().get("success").getAsBoolean()) {
            final JsonObject steamData = gameJson.get().getAsJsonObject("data");
            final String gameName = steamData.get("name").getAsString();
            game.setName(gameName);
            final PriceEntry priceEntry = addPriceEntry(game, steamData);
            game.setLowestPriceEntry(priceEntry);
            gameDao.saveOrUpdate(game);
        }
    }

    @Override
    public Game findGameById(String id) {
        return gameDao.findById(Long.valueOf(id));
    }

    @Override
    public List<Game> findAllGames() {
        return gameDao.findAll();
    }

    @Override
    @Scheduled(cron = "0 0 18 * * ?")
    public void updatePrices() {
        final List<Game> allGames = this.findAllGames();
        for (Game game : allGames) {
            final Optional<JsonObject> gameJson = requestSteamForGameDetails(game.getSteamId());
            if (gameJson.isPresent() && gameJson.get().get("success").getAsBoolean()) {
                final JsonObject steamData = gameJson.get().getAsJsonObject("data");
                final PriceEntry priceEntry = addPriceEntry(game, steamData);
                if(priceEntry.getPrice() < game.getLowestPriceEntry().getPrice()){
                    game.setLowestPriceEntry(priceEntry);
                    gameDao.saveOrUpdate(game);
                }
            }
        }
    }

    private PriceEntry addPriceEntry(Game game, JsonObject steamData) {
        final JsonObject priceOverview = steamData.getAsJsonObject("price_overview");
        final String currency = priceOverview.get("currency").getAsString();
        final long price = priceOverview.get("final").getAsLong();
        final PriceEntry priceEntry = new PriceEntry();
        priceEntry.setGame(game);
        priceEntry.setCurrency(currency);
        priceEntry.setPrice(price);
        priceEntry.setPriceDate(LocalDateTime.now());
        priceEntryService.addPriceEntry(priceEntry);
        return priceEntry;
    }


    private Optional<JsonObject> requestSteamForGameDetails(Long steamGameId) {
        JsonObject steamGame = null;
        try {
            URL url = new URL(gameDetailsUrl + steamGameId);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            if (request.getResponseCode() == 200) {
                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                steamGame = root.getAsJsonObject().getAsJsonObject(String.valueOf(steamGameId));
            }
        } catch (IOException e) {
        }
        return Optional.ofNullable(steamGame);
    }
}
