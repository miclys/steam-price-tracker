package com.lyson.steam.price.tracker.controller;

import com.lyson.steam.price.tracker.domain.Game;
import com.lyson.steam.price.tracker.domain.PriceEntry;
import com.lyson.steam.price.tracker.service.SteamGameService;
import com.lyson.steam.price.tracker.service.SteamPriceEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by lyson on 31.05.2016.
 */
@RestController
@RequestMapping("/api/games")
public class SteamGamesApiController {

    @Autowired
    private SteamGameService steamGameService;
    @Autowired
    private SteamPriceEntryService steamPriceEntryService;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public void addGame(@RequestBody Game game) throws IOException {
        steamGameService.addGame(game);
    }


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Game> getGames() {
        return steamGameService.findAllGames();
    }

    @RequestMapping(path = "{gameId}", method = RequestMethod.GET)
    @ResponseBody
    public Game getGame(@PathVariable("gameId") String gameId) {
        return steamGameService.findGameById(gameId);
    }

    @RequestMapping(path = "{gameSteamId}/prices", method = RequestMethod.GET)
    @ResponseBody
    public List<PriceEntry> getPrices(@PathVariable("gameSteamId") String gameSteamId) {
        return steamPriceEntryService.findPricesForGame(gameSteamId);
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    @ResponseBody
    public void updateGamesPrices() {
        steamGameService.updatePrices();
    }

}