package com.lyson.steam.price.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.util.TimeZone;

@SpringBootApplication
@EntityScan(
        basePackageClasses = {SteamPriceTrackerApplication.class, Jsr310JpaConverters.class}
)
public class SteamPriceTrackerApplication {


    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(SteamPriceTrackerApplication.class, args);
    }
}
