package com.lyson.steam.price.tracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lyson.steam.price.tracker.domain.Game;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by lyson on 29.05.2016.
 */
@Entity
@Table(name = "price_entry", schema = "steam")
public class PriceEntry {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.ALL})
    private Game game;

    private Long price;

    @Column(length = 3)
    private String currency;

    private LocalDateTime priceDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public LocalDateTime getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(LocalDateTime priceDate) {
        this.priceDate = priceDate;
    }
}
