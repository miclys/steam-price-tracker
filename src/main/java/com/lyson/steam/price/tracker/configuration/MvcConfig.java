package com.lyson.steam.price.tracker.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by lyson on 30.05.2016.
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("games");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/games").setViewName("games");
        registry.addViewController("/admin").setViewName("admin");
    }

}
