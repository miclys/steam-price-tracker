package com.lyson.steam.price.tracker.service;

import com.lyson.steam.price.tracker.domain.PriceEntry;

import java.util.List;

/**
 * Created by lyson on 01.06.2016.
 */
public interface PriceEntryService {
    void addPriceEntry(PriceEntry priceEntry);
    List<PriceEntry> findPricesForGame(String gameSteamId);
}
