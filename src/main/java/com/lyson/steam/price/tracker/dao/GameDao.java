package com.lyson.steam.price.tracker.dao;

import com.lyson.steam.price.tracker.domain.Game;

import java.util.List;

/**
 * Created by lyson on 31.05.2016.
 */
public interface GameDao {
    void saveOrUpdate(Game g);
    Game findById(Long id);
    List<Game> findAll();
}
