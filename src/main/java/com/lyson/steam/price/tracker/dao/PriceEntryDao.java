package com.lyson.steam.price.tracker.dao;

import com.lyson.steam.price.tracker.domain.PriceEntry;

import java.util.List;

/**
 * Created by lyson on 01.06.2016.
 */
public interface PriceEntryDao {
    void add(PriceEntry pe);
    List<PriceEntry> findPricesForGame(Long gameSteamId);
}
