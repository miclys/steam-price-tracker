package com.lyson.steam.price.tracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by lyson on 29.05.2016.
 */
@Entity
@Table(name = "game", schema = "steam")
public class Game {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private Long steamId;

    private String name;

    @JsonIgnore
    private boolean deleted;

    @OneToOne
    private PriceEntry lowestPriceEntry;

    @OneToMany(mappedBy = "game", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<PriceEntry> priceEntries = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSteamId() {
        return steamId;
    }

    public void setSteamId(Long steamId) {
        this.steamId = steamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PriceEntry getLowestPriceEntry() {
        return lowestPriceEntry;
    }

    public void setLowestPriceEntry(PriceEntry lowestPriceEntry) {
        this.lowestPriceEntry = lowestPriceEntry;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
