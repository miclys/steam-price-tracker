package com.lyson.steam.price.tracker.service;

import com.lyson.steam.price.tracker.domain.Game;

import java.util.List;

/**
 * Created by lyson on 01.06.2016.
 */
public interface GameService {
    void addGame(Game game);
    Game findGameById(String id);
    List<Game> findAllGames();
    void updatePrices();
}
