package com.lyson.steam.price.tracker.dao;

import com.lyson.steam.price.tracker.domain.PriceEntry;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lyson on 31.05.2016.
 */
@Repository
@Transactional
public class HibernatePriceEntryDao implements PriceEntryDao {

    private final SessionFactory sf;

    @Autowired
    public HibernatePriceEntryDao(SessionFactory sf) {
        this.sf=sf;
    }

    @Override
    public void add(PriceEntry pe) {
        sf.getCurrentSession().saveOrUpdate(pe);
    }

    @Override
    public List<PriceEntry> findPricesForGame(Long gameSteamId) {
        final Query query = sf.getCurrentSession().createQuery("from PriceEntry where game.steamId=:gameSteamId order by priceDate");
        query.setParameter("gameSteamId", gameSteamId);
        return query.list();
    }
}
