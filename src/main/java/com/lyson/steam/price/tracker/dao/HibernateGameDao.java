package com.lyson.steam.price.tracker.dao;

import com.lyson.steam.price.tracker.domain.Game;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lyson on 31.05.2016.
 */
@Repository
@Transactional
public class HibernateGameDao implements GameDao {

    private final SessionFactory sf;

    @Autowired
    public HibernateGameDao(SessionFactory sf) {
        this.sf=sf;
    }

    @Override
    public void saveOrUpdate(Game g) {
        sf.getCurrentSession().saveOrUpdate(g);
    }

    @Override
    public Game findById(Long id) {
        Query query = sf.getCurrentSession().
                createQuery("from Game where steamId=:id");
        query.setParameter("id", id);
        Game game = (Game) query.uniqueResult();
        return game;
    }

    @Override
    public List<Game> findAll() {
        final Query query = sf.getCurrentSession().createQuery("from Game order by name");
        return query.list();
    }
}
